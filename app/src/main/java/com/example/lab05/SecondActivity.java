package com.example.lab05;

import android.net.Uri;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    private TextView mainNamer;
    private Button onButtonClick;
    private Button backButton;
    Button buttonNumber;
    Button buttonWeb;
    Button buttonAddress;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        onButtonClick = findViewById(R.id.onButtonClick);
        backButton = findViewById(R.id.backbutton);
        mainNamer = findViewById(R.id.secondname);
        buttonWeb = (Button) findViewById(R.id.button4);
        buttonAddress = (Button) findViewById(R.id.button);
        buttonNumber = (Button) findViewById(R.id.button3);
        String name = getIntent().getStringExtra("keyname");
        mainNamer.setText(name);

        buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.linkedin.com/in/ignatkim/"));
                startActivity(intent);
            }
        });

        buttonAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo: 43.000000, -75.000000"));
                startActivity(intent);
            }
        });
        buttonNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel: +996557225577"));
                startActivity(intent);
            }
        });

        onButtonClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText secondName = (EditText) findViewById(R.id.secondname);
                String stringToPassBack = secondName.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("keyName", stringToPassBack);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView imageView = (ImageView) findViewById(R.id.image_s);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int res_image = bundle.getInt("IUCA");
            imageView.setImageResource(res_image);
        }


    }
}
